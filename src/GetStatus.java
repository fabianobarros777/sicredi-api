import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetStatus {
    public static void main(String[] args) {
        try {
            // URL para enviar a solicitação
            URL url = new URL("https://dummyjson.com/test");

            // Abrir uma conexão
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Definir o método de solicitação
            connection.setRequestMethod("GET");

            // Obter o código de resposta
            int responseCode = connection.getResponseCode();
            System.out.println("Response Code: " + responseCode);

            // Ler a resposta
            BufferedReader reader;
            if (responseCode == HttpURLConnection.HTTP_OK) {
                // Se o código de resposta for OK (200), ler o corpo da resposta
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            } else if (responseCode == HttpURLConnection.HTTP_NOT_FOUND) {
                // Se o recurso não for encontrado (404), exibir mensagem de erro
                System.err.println("Resource not found.");
                reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            } else {
                // Se outro código de resposta de erro, ler o fluxo de erro
                reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            }

            // Imprimir a resposta
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();
            System.out.println("Response Body: " + response.toString());

            // Desconectar a conexão
            connection.disconnect();
        } catch (java.io.IOException e) {
            System.err.println("IO Exception: " + e.getMessage());
        }
    }
}
