import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetProducts {
    public static void main(String[] args) {
        try {
            // Create URL
            URL url = new URL("https://dummyjson.com/products");

            // Create connection
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            // Get the response
            int responseCode = connection.getResponseCode();

            // Read response body with line breaks
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // Print response with formatted objects
            String responseString = response.toString();
            responseString = responseString.replaceAll("\\{", "{\n\t");
            responseString = responseString.replaceAll("\\},", "\n}\n");
            responseString = responseString.replaceAll("\"images\": \\[", "\"images\": [\n\t\t");
            responseString = responseString.replaceAll("\\],", "\n\t],");
            System.out.println(responseString);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
