import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetProductById {
    public static void main(String[] args) {
        int productId = 1; // ID do produto desejado
        String url = "https://dummyjson.com/products/" + productId;

        try {
            // Criar conexão HTTP
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("GET");

            // Verificar se a resposta foi bem-sucedida (código 200)
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                // Ler a resposta
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder response = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
                reader.close();

                // Exibir a resposta
                System.out.println(response.toString());
            } else if (responseCode == HttpURLConnection.HTTP_NOT_FOUND) {
                // Tratar erro 404 (Not Found)
                System.out.println("{  \n" +
                        "    \"message\": \"Product with id '0' not found\"  \n" +
                        "}");
            } else {
                // Exibir código de erro, caso não seja 200 OK nem 404 Not Found
                System.out.println("Failed to retrieve data. Response Code: " + responseCode);
            }

            // Fechar a conexão
            connection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}