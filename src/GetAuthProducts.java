import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class GetAuthProducts {
    public static void main(String[] args) {
        try {
            HttpClient httpClient = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create("https://dummyjson.com/auth/products"))
                    .header("Content-Type", "application/json")
                    .header("Authorization", "Substitua YOUR_TOKEN_HERE") // Substitua YOUR_TOKEN_HERE pelo seu token real
                    .GET()
                    .build();

            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

            // Verificando o código de resposta
            if (response.statusCode() == 200) {
                System.out.println(response.body());
            } else if (response.statusCode() == 403) {
                JSONObject jsonError = new JSONObject(response.body());
                System.out.println("{\t\t\n" +
                        "\tmessage: \"Authentication Problem\"\n" +
                        "}");
            } else if (response.statusCode() == 401) {
                JSONObject jsonError = new JSONObject(response.body());
                System.out.println("{\n" +
                        "    \"name\": \"JsonWebTokenError\",\n" +
                        "    \"message\": \"Invalid/Expired Token!\"\n" +
                        "}");
            } else {
                System.out.println("Request failed with status code: " + response.statusCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
