import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class AuthLogin {
    public static void main(String[] args) {
        String token = null;
        try {
            // URL da solicitação
            URL url = new URL("https://dummyjson.com/auth/login");

            // Abrir conexão
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Configurar a solicitação como POST
            connection.setRequestMethod("POST");

            // Configurar cabeçalhos
            connection.setRequestProperty("Content-Type", "application/json");

            // Habilitar envio de dados
            connection.setDoOutput(true);

            // Corpo da solicitação
            String requestBody = "{\"username\": \"kminchelle\", \"password\": \"0lelplR\"}";

            // Enviar dados
            connection.getOutputStream().write(requestBody.getBytes());

            // Obter código de resposta
            int responseCode = connection.getResponseCode();
            String responseMessage = connection.getResponseMessage();

            if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_CREATED) {
                // Ler resposta
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // Exibir resposta
                System.out.println(response.toString());

                // Extrair token da resposta
                token = new JSONObject(response.toString()).getString("token");
            } else {
                // Exibir mensagem de erro
                System.out.println("Error Response - " + responseCode + " " + responseMessage);

                // Ler resposta de erro, se houver
                BufferedReader errorIn = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                String errorInputLine;
                StringBuilder errorResponse = new StringBuilder();
                while ((errorInputLine = errorIn.readLine()) != null) {
                    errorResponse.append(errorInputLine);
                }
                errorIn.close();

                // Exibir resposta de erro
                System.out.println(errorResponse.toString());
            }

            // Fechar conexão
            connection.disconnect();
        } catch (java.net.MalformedURLException e) {
            System.err.println("URL malformada: " + e.getMessage());
        } catch (java.io.IOException e) {
            System.err.println("Erro de I/O: " + e.getMessage());
        } catch (Exception e) {
            System.err.println("Erro desconhecido: " + e.getMessage());
        }
    }
}