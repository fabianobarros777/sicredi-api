import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class AddProducts {
    public static void main(String[] args) throws java.net.URISyntaxException {
        try {
            // Corpo da solicitação em formato JSON
            String requestBody = "{" +
                    "\"title\": \"Perfume Oil\"," +
                    "\"description\": \"Mega Discount, Impression of A...\"," +
                    "\"price\": 13," +
                    "\"discountPercentage\": 8.4," +
                    "\"rating\": 4.26," +
                    "\"stock\": 65," +
                    "\"brand\": \"Impression of Acqua Di Gio\"," +
                    "\"category\": \"fragrances\"," +
                    "\"thumbnail\": \"https://i.dummyjson.com/data/products/11/thumnail.jpg\"" +
                    "}";

            // Criando a solicitação HTTP POST
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create("https://dummyjson.com/products/add"))
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                    .build();

            // Enviando a solicitação e recebendo a resposta
            HttpClient httpClient = HttpClient.newHttpClient();
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

            // Verificando o código de resposta
            if (response.statusCode() == 200) {
                System.out.println(response.body());
            } else {
                System.out.println("Falha ao criar o produto. Código de resposta: " + response.statusCode());
                System.out.println("Corpo da resposta:");
                System.out.println(response.body());
            }
        } catch (java.io.IOException e) {
            System.err.println("Erro de I/O ao enviar a solicitação: " + e.getMessage());
        } catch (java.lang.InterruptedException e) {
            System.err.println("Erro ao aguardar a resposta: " + e.getMessage());
        } catch (Exception e) {
            System.err.println("Erro desconhecido: " + e.getMessage());
        }
    }
}